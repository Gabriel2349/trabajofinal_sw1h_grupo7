#pragma once
#include <iostream>
#include <Windows.h>
#include <iomanip>
#include <math.h>

void startGame();
void endGame();

void displayMainMenu();
void displayInstructions();
void displayGameOver();
void displayVictory();

void printLevel(int levelNumber);
void mathProblem();
void mathSolution();

void updateScore();
void updateHealth();
void saveProgress();
void loadProgress();

void drawPlayer(int x, int y);
void drawEnemy(int x, int y);
void drawBorder();
void drawObstacle(int x, int y);


void gotoxy(int x, int y); 
void changeColor(int color); 
void clear(); 
